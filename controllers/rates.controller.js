const Rates = require("../models/rates");
const Rate = require("../models/rates");

exports.getRateCategories = (req, res) => {
    Rates.getCategories((err, rateCategories) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "No rate categories found"
                });
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving rate categories"
                });
            }
        }
        else {
            res.status(200).send(rateCategories);
        }
    })
}

exports.getTypeAndConfig = (req, res) => {
    Rates.getTypeAndConfig((err, rates) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "No rate categories found"
                });
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving rate categories"
                });
            }
        }
        else {
            let rateItems = [];
            rates.forEach(item => {
                let type = item.type;
                if (type === "Generation And Transmission") {
                    type = "G&T";
                }
                rateItems.push({
                    categoryId: item.categoryid,
                    typeId: item.typeid,
                    configId: item.configid,
                    typeName: `${type} > ${item.config}`
                });
            });
            res.status(200).send(rateItems);
        }
    })
}

exports.getRatesPagination = (req, res) => {
    let searchString = req.body.searchString || null;
    Rates.getRatesByCategoryPagination(req.body.categoryid, searchString, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "No rates found"
                });
            }
            else {
                res.status(500).send({
                    message: err.message || "Error retrieving rates"
                });
            }
        }
        else {
            let finalRates = [];
            console.log(res);
            data.forEach(item => {
                let itemCreated = false;
                finalRates.forEach(resultItem => {
                    if (resultItem.type === item.type && resultItem.config === item.config) {
                        if (resultItem.new_appconfigid < item.appconfigid) {
                            resultItem.old_value = resultItem.new_value;
                            resultItem.old_appconfigid = resultItem.new_appconfigid;
                            resultItem.new_value = item.value;
                            resultItem.new_appconfigid = item.appconfigid;
                        }
                        else if (resultItem.old_appconfigid < item.appconfigid) {
                            resultItem.old_value = item.value;
                            resultItem.old_appconfigid = item.appconfigid;
                        }
                        itemCreated = true;
                    }
                });
                if (!itemCreated) {
                    finalRates.push({
                        type: item.type,
                        config: item.config,
                        old_value: 0,
                        new_value: item.value,
                        old_appconfigid: -1,
                        new_appconfigid: item.appconfigid
                    })
                }
            })
            let page = req.body.page || 1;
            const pageSize = req.body.pageSize || 10;
            const totalPages = Math.ceil(finalRates.length / pageSize);
            if (page > totalPages) {
                page = totalPages;
            }
            const results = finalRates.slice((page - 1) * pageSize, (page * pageSize));
            console.log(results);
            res.status(200).send({
                totalPages: totalPages,
                page: page,
                pageSize: pageSize,
                items: results
            });
        }
    })
}

exports.addRate = (req, res) => {
    Rate.addRate(req.body.appconfigid, req.body.value, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error adding new rate"
            })
        }
        else {
            res.status(200).send({
                message: "New rate added"
            })
        }
    })
}