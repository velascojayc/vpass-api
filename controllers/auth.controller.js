const AppUser = require("../models/app_user");
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const env = require("../config/env.config");
const { updateById } = require("../models/app_user");

//Verifies if the password matches what is in the database
exports.verifyPassword = (req, res, next) => {
    if (!req.body) {
      res.status(400).send({message: "Content cannot be empty"});
    }
    AppUser.findByUser(req.body.username, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({message: `User Not found`});
        } else {
            res.status(500).send({message: "Error retrieving user"});
        }
      } else {
        let passwordSplit = data.password.split('|');
        let salt = passwordSplit[0];
        let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
        console.log(hash);
        if (hash === passwordSplit[1]) {
            req.body.id = data.id;
            return next();
        } else {
          res.status(401).send({message: "Invalid username or password"});
        }
      }
    });
}

//Verifies if body contains username and password fields
exports.validateAuthFields = (req, res, next) => {
    if (req.body) {
        if (!req.body.username) {
            res.status(400).send({
                message: "Missing username"
            });
        } else
        if (!req.body.password) {
            res.status(400).send({
                message: "Missing password"
            });
        } else {
            return next();
        }
    } else {
        res.status(400).send({
            message: "Missing email and password"
        });
    }
}

//Creates JWT token for authorization
exports.login = (req, res) => {
    console.log('auth', req.body);
    try {
        const tokenDetails = {
            id: req.body.id,
            username: req.body.username,
            password: req.body.password
        };
        let token = jwt.sign(tokenDetails, env.JWT_SECRET);
        AppUser.findBy(req.body.id, (err, data) => {
            if (err) {
                res.status(500).send({message: "Error checking user"})
            }
            else {
                AppUser.updateToken(req.body.id, token, (err, d) => {
                    if (err) {
                        res.status(500).send({message: "Error with token creation"})
                    }
                    else {
                        AppUser.findRole(req.body.id, (err, role) => {
                            if (err) {
                                res.status(500).send({message: "Error retrieving role"})
                            }
                            else {
                                res.status(200).send({
                                    token: token,
                                    user: {
                                            id: req.body.id,
                                            username: data.username,
                                            email: data.email,
                                            role: role.name,
                                            firstname: data.firstname,
                                            lastname: data.lastname,
                                            street1: data.street1,
                                            street2: data.street2,
                                            region: data.region,
                                            province: data.province,
                                            city: data.city,
                                            barangay: data.barangay,
                                            created_at: data.created_at,
                                            updated_at: data.updated_at
                                    }
                                });
                            }
                        })
                    }
                })
            }
        })
    }
    catch (err) {
        res.status(500).send({
            message: err
        });
    }
}

//Deletes access token in db
exports.logout = (req, res) => {
    AppUser.deleteToken(req.jwt.id, (err, data) => {
        if (err) {
            res.status(500).send({
                message: "Error logging out"
            });
        }
        else {
            res.status(200).send({
                message: "Successfully logged out"
            });
        }
    })
}