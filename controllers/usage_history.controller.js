const UsageHistory = require("../models/usage_history");
const multer = require("multer");
const multerS3 = require("multer-s3");
const aws = require("aws-sdk");
const env = require("../config/env.config");

// S3 Bucket upload
const s3 = new aws.S3();
const s3Upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: env.S3_BUCKET,
    key: function(req, file, cb) {
      const fileName = Date.now() + file.originalname;
      cb(null, env.S3_FOLDER + fileName);
    }
  })
}).single("meter_img");

const s3SignedUrl = function(fileName) {
  return new Promise(function(resolve, reject) {
    const url = s3.getSignedUrl('getObject', {
      Bucket: env.S3_BUCKET,
      Key: fileName,
      Expires: 60 * 60
    }, function(err, url) {
      resolve(url);
    });
  });
}

exports.create = (req, res) => {
  if (!req.body) {
      res.status(400).send({message: "Content can not be empty!"});
  }
  s3Upload(req, res, function(err) {
    if (err) {
      console.log('error', err);
      res.status(500).send({
        message: 'Error uploading image'
      });
    }
    else {
      let location = null;
      if (req.file && req.file.key) {
        location = req.file.key;
      }
      UsageHistory.findByUserAccountId(req.body.useraccountid, (err, data) => {
        let past_reading = 0;
        if (data && data.length > 0) {
          latestRead = data[data.length - 1];
          if (latestRead.current_reading != null) {
            past_reading = latestRead.current_reading;
          }
        }
        let usage = parseInt(req.body.current_reading) - parseInt(past_reading);
        addRates(null, usage).then(x => {
          const usageHistory = new UsageHistory({
            useraccountid: req.body.useraccountid,
            usage: usage,
            past_reading: past_reading,
            current_reading: parseInt(req.body.current_reading),
            usage_month: req.body.usage_month,
            usage_year: req.body.usage_year,
            captured_at: req.body.captured_at,
            meter_img: location,
            amount_due: x.total.toFixed(2),
            received_by: req.body.received_by || null
          });
    
          UsageHistory.create(usageHistory, (err, data) => {
            if (err) 
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the usage."
                });
            else {
              // data.rate_value = rateValue;
              s3SignedUrl(location).then(url => {
                data.meter_img = url;
                data.rates = x.rates;
                res.status(200).send(data);
              });
            }
          });
        });
      });
    }
  })
};

exports.checkExisting = (req, res) => {
  UsageHistory.validateExisting(req.body.electricmeterid, req.body.usage_month, req.body.usage_year, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "No duplicate entries found"
        });
      }
      else {
        res.status(500).send({
          message: "Error checking for duplicates"
        });
      }
    }
    else {
      res.status(200).send({
        message: "A usage history entry has been found for period " + req.body.usage_month + " " + req.body.usage_year
      })
    }
  })
}

exports.findOne = (req, res) => {
    UsageHistory.findBy(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({message: `Usage Not found`});
            } else {
                res.status(500).send({message: "Error retrieving Usage"});
            }
        } else {
          addRates(data.captured_at, data.usage).then(x => {
            data.rates = x.rates;
            s3SignedUrl(data.meter_img).then(url => {
              data.meter_img = url;
              res.status(200).send(data);
            });
          });
        }
    });
};

exports.findAll = (req, res) => {
    UsageHistory.getAll((err, data) => {
        console.log(data);
        if (err) 
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving usage."
            });
         else {
            const promises = [];
            data.forEach(item => {
              promises.push(addRates(item.captured_at, item.usage).then(x => {
                item.rates = x.rates;
              }));
              let promise = new Promise(function(resolve, reject) {
               console.log(item.meter_img);
               s3SignedUrl(item.meter_img).then(url => {
                 item.meter_img = url;
                 resolve();
               });
              })
              promises.push(promise);
            });
            Promise.all(promises).then(result => {
             res.status(200).send(data);
            });
         }
    });
};

exports.search = (req, res) => {
  console.log('in search route');
  if (!req.params && !req.params.searchString) {
    res.status(400).send({
      message: "No search parameters supplied"
    });
  }
  else {
    console.log('search params', req.params.searchString);
    UsageHistory.search(req.params.searchString, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: "No usage history found"
          });
        }
        else {
          res.status(500).send({
            message: "Error restrieving usage history"
          });
        }
      }
      else {
        const promises = [];
        data.forEach(item => {
          promises.push(addRates(item.captured_at, item.usage).then(x => {
            item.rates = x.rates;
          }));
          let promise = new Promise(function(resolve, reject) {
          s3SignedUrl(item.meter_img).then(url => {
            item.meter_img = url;
            resolve();
          });
          })
          promises.push(promise);
        });
        Promise.all(promises).then(result => {
          res.status(200).send(data);
        });
      }
    });
  }
}

exports.findByAccountDate = (req, res) => {
  UsageHistory.findByAccountMonthYear(req.params.accountnumber, req.params.usage_month, req.params.usage_year, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "No usage history found for user"
        });
      }
      else {
        res.status(500).send({
          message: "Error retrieving usage history for user"
        });
      }
    }
    else {
      const promises = [];
        data.forEach(item => {
          promises.push(addRates(item.captured_at, item.usage).then(x => {
            item.rates = x.rates;
          }));
          let promise = new Promise(function(resolve, reject) {
          s3SignedUrl(item.meter_img).then(url => {
            item.meter_img = url;
            resolve();
          });
          })
          promises.push(promise);
        });
        Promise.all(promises).then(result => {
          res.status(200).send(data);
        });
    }
  });
}

exports.findByUserId = (req, res) => {
  console.log('in findbyuserid route');
  UsageHistory.findByUserId(req.jwt.id, req.params.usage_month, req.params.usage_year, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "No usage history found for user"
        });
      }
      else {
        res.status(500).send({
          message: "Error retrieving usage history for user"
        });
      }
    }
    else {
      const promises = [];
        data.forEach(item => {
          promises.push(addRates(item.captured_at, item.usage).then(x => {
            item.rates = x.rates;
          }));
          let promise = new Promise(function(resolve, reject) {
          s3SignedUrl(item.meter_img).then(url => {
            item.meter_img = url;
            resolve();
          });
          })
          promises.push(promise);
        });
        Promise.all(promises).then(result => {
          res.status(200).send(data);
        });
    }
  });
}

exports.findByMeterId = (req, res) => {
  if (!req.params && !req.params.meterId) {
    res.status(400).send({
      message: "Invalid or missing parameters"
    });
  }
  else {
    UsageHistory.findByMeterId(req.params.meterId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: "No usage history found for user"
          });
        }
        else {
          res.status(500).send({
            message: "Error retrieving usage history for user"
          });
        }
      }
      else {
        const promises = [];
        data.forEach(item => {
          promises.push(addRates(item.captured_at, item.usage).then(x => {
            item.rates = x.rates;
          }));
          let promise = new Promise(function(resolve, reject) {
          s3SignedUrl(item.meter_img).then(url => {
            item.meter_img = url;
            resolve();
          });
          })
          promises.push(promise);
        });
        Promise.all(promises).then(result => {
          res.status(200).send(data);
        });
      }
    });
  }
}

exports.update = (req, res) => {
    if (!req.body) {
        res.status(400).send({message: "Content can not be empty!"});
    }
    s3Upload(req, res, function(err) {
      if (err) {
        console.log('error', err);
        res.status(500).send({
          message: 'Error uploading image'
        });
      }
      else {
        if (req.file && req.file.location) {
          req.body.meter_img = req.file.location;
        }
        addRates(null, req.body.usage).then(x => {
          const rates = x.rates;
          let total = x.total;
          let usageHistory = new UsageHistory({
            useraccountid: req.body.useraccountid,
            usage: req.body.usage,
            past_reading: req.body.past_reading,
            current_reading: req.body.current_reading,
            usage_month: req.body.usage_month,
            usage_year: req.body.usage_year,
            captured_at: req.body.captured_at,
            amount_due: total.toFixed(2),
            received_by: req.body.received_by
          });
          if (req.file && req.file.location) {
            usageHistory.meter_img = req.file.location;
          }
          UsageHistory.updateById(req.params.id, usageHistory, (err, data) => {
            console.log(err);
            if (err) {
              if (err.kind === "not_found") {
                res.status(404).send({
                  message: `Usage Not found.`
                });
              } else {
                res.status(500).send({
                  message: "Error updating usage"
                });
              }
            }
            else {
              data.rates = rates;
              if (usageHistory.meter_img) {
                s3SignedUrl(location).then(url => {
                  data.meter_img = url;
                  res.status(200).send(data);
                });
              }
              else {
                res.status(200).send(data);
              }
            }
          });
        });
      }
    });
};

exports.delete = (req, res) => {
    UsageHistory.remove(req.params.id, (err, data) =>{
        if (err) {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: `Usage Not found.`
              });
            } else {
              res.status(500).send({
                message: "Could not delete usage"
              });
            }
          } else res.status(200).send({ message: `Usage was deleted successfully!` });
    });
};

exports.findByAccountNumber = (req, res) => {
  UsageHistory.findByUserAccountNumber(req.params.accountnumber, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No usage history found for account number`
        })
      }
      else {
        res.status(500).send({
          message: "Error retrieving ussage history"
        })
      }
    }
    else {
      const promises = [];
      data.forEach(item => {
        promises.push(addRates(item.captured_at, item.usage).then(x => {
          item.rates = x.rates;
        }));
        let promise = new Promise(function(resolve, reject) {
          console.log(item.meter_img);
          s3SignedUrl(item.meter_img).then(url => {
            item.meter_img = url;
            resolve();
          });
        })
        promises.push(promise);
      });
      Promise.all(promises).then(result => {
        res.status(200).send(data);
      });
    }
  });
}

const addRates = function(date, usage) {
  return new Promise(function(resolve, reject) {
    const rateDate = new Date(date).toISOString().slice(0, 19).replace('T', ' ') || new Date().toISOString().slice(0, 19).replace('T', ' ');
    UsageHistory.getRates(rateDate, (err, rateData) => {
      let rates = [];
      let categories = [];
      let type = [];
      let total = 0;
      rateData.forEach(item => {
        let amount = parseFloat(item.value) * parseFloat(usage);
        total = total + amount;
        if (categories.indexOf(item.category) === -1) {
          categories.push(item.category);
          rates.push({
            category: item.category,
            categoryTypes: [
              {
                name: item.type,
                types: [
                  {
                    name: item.rate,
                    charge: item.value,
                    amount: amount.toFixed(2)
                  }
                ]
              }
            ]
          });
          type = [item.type];
        }
        else if (type.indexOf(item.type) === -1) {
          type.push(item.type)
          rates.forEach(categoryItem => {
            if (categoryItem.category === item.category) {
              categoryItem.categoryTypes.push({
                name: item.type,
                types: [{
                  name: item.rate,
                  charge: item.value,
                  amount: amount.toFixed(2)
                }]
              })
            }
          })
        }
        else {
          rates.forEach(categoryItem => {
            if (categoryItem.category === item.category) {
              categoryItem.categoryTypes.forEach(typeItem => {
                if (typeItem.name === item.type) {
                  typeItem.types.push({
                    name: item.rate,
                    charge: item.value,
                    amount: amount.toFixed(2)
                  })
                }
              })
            }
          })
        }
      });
      resolve({
        total: total,
        rates: {
          categories: rates
        }
      });
    });
  });
}
