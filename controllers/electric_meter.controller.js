const ElectricMeter = require("../models/electric_meter");
const AppUser = require("../models/app_user");

exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({message: "Content can not be empty!"});
    }

    const electricMeter = new ElectricMeter({
        meter_number: req.body.meter_number,
        gps_lat: req.body.gps_lat,
        gps_lng: req.body.gps_lng
    });

    ElectricMeter.create(electricMeter, (err, data) => {
        if (err) 
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user."
            });
         else 
            res.send(data);
    });
};

exports.addMeterToAccount = (req, res) => {
  ElectricMeter.findByMeterNumber(req.body.meter_number, (err, meterData) => {
    if (err) {
      if (err.kind === "not_found") {
        AppUser.findByUserAccountNumber(req.body.account_number, (err, userData) => {
          if (err) {
            if (err.kind === "not_found") {
              res.status(400).send({
                message: "User Account not found"
              });
            }
            else {
              res.status(500).send({
                message: err.message || "Some error occured while assigning meter to account number"
              })
            }
          }
          else {
            console.log(userData);
            const electricMeter = new ElectricMeter({
              meter_number: req.body.meter_number,
              gps_lat: req.body.gps_lat || null,
              gps_lng: req.body.gps_lng || null
            });
            ElectricMeter.create(electricMeter, (err, createRes) => {
              if (err) {
                res.status(500).send({
                  message: err.message || "Some error occured while assigning meter to account number"
                })
              }
              else {
                ElectricMeter.assignMeterToAccount(createRes.id, userData.useraccountid, (err, data) => {
                  if (err) {
                    res.status(500).send({
                      message: err.message || "Some error occured while assigning meter to account number"
                    })
                  }
                  else {
                    res.status(200).send({
                      message: "Meter number has been assigned to user account"
                    })
                  }
                })
              }
            })
          }
        })
      }
      else {
        res.status(500).send({
          message: err.message || "Some error occured while assigning meter to account number"
        })
      }
    }
    else {
      console.log(meterData);
      res.status(500).send({
        message: "Meter number already exists"
      })
    }
  })
}

exports.findOne = (req, res) => {
    ElectricMeter.findBy(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({message: `Electric Meter Not found`});
            } else {
                res.status(500).send({message: "Error retrieving electric meter"});
            }
        } else 
            res.send(data);
        
    });
};

exports.findByUserName = (req, res) => {
  if (!req.params.username) {
    res.status(400).send({
      message: "No username specified"
    });
  }
  ElectricMeter.findByUserName(req.params.username, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "No meter found for user"
        })
      }
      else {
        res.status(500).send({
          message: "Error retrieving meter details"
        })
      }
    }
    else {
      res.status(200).send(data);
    }
  })
}

exports.findByUserId = (req, res) => {
  ElectricMeter.findByUserId(req.jwt.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "No meters found for user"
        });
      }
      else {
        res.status(500).send({
          message: "Error retrieving user's meters"
        });
      }
    }
    else {
      res.status(200).send(data);
    }
  });
}

exports.findByMeterNumber = (req, res) => {
  if (!req.params.meternumber) {
    res.status(400).send({
      message: "No meter number specified"
    });
  }
  ElectricMeter.findByMeterNumber(req.params.meternumber, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "Could not find meter number"
        });
      }
      else {
        res.status(500).send({
          message: "Error retrieving meter details"
        })
      }
    }
    else {
      res.status(200).send(data);
    }
  })
}

exports.findAll = (req, res) => {
    ElectricMeter.getAll((err, data) => {
        console.log(data);
        if (err) 
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving electric meters."
            });
         else 
            res.send(data);
    });
};

exports.paginationSearch = (req, res) => {
  const searchString = req.body.searchString || null;
  ElectricMeter.search(searchString, (err, data) => {
    if (err) 
      res.status(500).send({
          message: err.message || "Some error occurred while retrieving electric meters."
      });
    else {
      let page = req.body.page || 1;
      const pageSize = req.body.pageSize || 10;
      const totalPages = Math.ceil(data.length / pageSize);
      if (page > totalPages) {
        page = totalPages;
      }
      const results = data.slice((page - 1) * pageSize, (page * pageSize));
      res.status(200).send({
        totalPages: totalPages,
        page: page,
        pageSize: pageSize,
        items: results
      });
    }
  })
}

exports.update = (req, res) => {
    if (!req.body) {
        res.status(400).send({message: "Content can not be empty!"});
    }

    ElectricMeter.updateById(req.params.id, new ElectricMeter(req.body), (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: `Electric Meter Not found.`
              });
            } else {
              res.status(500).send({
                message: "Error updating electric meter"
              });
            }
          } else res.send(data);
    });
};

exports.delete = (req, res) => {
    ElectricMeter.remove(req.params.id, (err, data) =>{
        if (err) {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: `Electric Meter Not found.`
              });
            } else {
              res.status(500).send({
                message: "Could not delete Electric Meter"
              });
            }
          } else res.send({ message: `Electric Meter was deleted successfully!` });
    });
};
