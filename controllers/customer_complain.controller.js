const CustomerComplain = require("../models/customer_complain");

exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({message: "Content can not be empty!"});
    }

    const customerComplain = new CustomerComplain({
        useraccountid: req.body.useraccountid,
        title: req.body.title,
        description: req.body.description,
        create_at: Date.now(),
        status: req.body.status,
        assigned_to: req.body.assigned_to
    });

    CustomerComplain.create(customerComplain, req.jwt.id, (err, data) => {
        if (err) 
            res.status(500).send({
                message: err.message || "Some error occurred while creating the complain."
            });
         else 
            res.send(data);
    });
};

exports.updateHistory = (req, res) => {
  if (!req.body) {
    res.status(400).send({message: "Content can not be empty!"});
  }
  else {
    const notes = req.body.notes || null;
    CustomerComplain.updateHistory(req.jwt.id, req.body.customercomplainid, req.body.status, notes, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: "Complaint not found"
          });
        }
        else {
          res.status(500).send({
            message: err.message || "Erorr occured while updating complaint history"
          });
        }
      }
      else {
        res.status(200).send({
          message: "Successfully updated complaint history"
        });
      }
    })
  }
}

exports.getComplaintHistory = (req, res) => {
  CustomerComplain.getComplaintHistory(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "Complaint not found"
        });
      }
      else {
        res.status(500).send({
          message: err.message || "Erorr occured while updating complaint history"
        });
      }
    }
    else {
      let responseData = {
        accountnumber: data[0].accountnumber,
        title: data[0].title,
        description: data[0].description,
        status: data[0].status,
        notes: []
      }
      let notes = [];
      data.forEach(item => {
        const noteItem = {
          old_status: item.old_status,
          new_status: item.new_status,
          created_at: item.created_at,
          created_by: item.created_by
        }
        notes.push(noteItem);
      });
      responseData.notes = notes;
      res.status(200).send(responseData);
    }
  })
}

exports.search = (req, res) => {
  if (!req.params.search) {
    res.status(400).send({
      message: "No search term defined"
    });
  }
  CustomerComplain.search(req.params.search, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "No matching entries found"
        });
      }
      else {
        res.status(500).send({
          message: "Error searching for complaints"
        });
      }
    }
    else {
      res.status(200).send(data);
    }
  });
};

exports.paginationSearch = (req, res) => {
  const searchString = req.body.searchString || null;
  CustomerComplain.paginationSearch(searchString, (err, data) => {
    if (err) 
      res.status(500).send({
          message: err.message || "Some error occurred while retrieving users."
      });
    else {
      let page = req.body.page || 1;
      const pageSize = req.body.pageSize || 10;
      const totalPages = Math.ceil(data.length / pageSize);
      if (page > totalPages) {
          page = totalPages;
      }
      const results = data.slice((page - 1) * pageSize, (page * pageSize));
      console.log(results);
      res.status(200).send({
          totalPages: totalPages,
          page: page,
          pageSize: pageSize,
          items: results
      });
    }  
  })
}

exports.findOne = (req, res) => {
    CustomerComplain.findBy(req.params.id, (err, data) =>{
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({message: `Complain Not found`});
            } else {
                res.status(500).send({message: "Error retrieving complain"});
            }
        } else 
            res.send(data);
    });
};

exports.findAllByAppUserId = (req, res) => {
  CustomerComplain.findAllBy(req.params.useraccountid, (err, data) =>{
      if (err) {
          if (err.kind === "not_found") {
              res.status(404).send({message: `Complain Not found`});
          } else {
              res.status(500).send({message: "Error retrieving complain"});
          }
      } else 
          res.send(data);
  });
};

exports.findAll = (req, res) => {
    CustomerComplain.getAll((err, data) => {
        if (err) 
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving complains."
            });
         else 
            res.send(data);
    });
};

exports.findByUserName = (req, res) => {
  if (!req.params.username) {
    res.status(400).send({
      message: "No username specified"
    })
  }
  CustomerComplain.findByUserName(req.params.username, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({message: "No complaints found for user"})
      }
      else {
        res.status(500).send({message: "Error retrieving complaints for user"})
      }
    }
    else {
      res.status(200).send(data);
    }
  })
}

exports.findByUserId = (req, res) => {
  CustomerComplain.findByUserId(req.jwt.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "No Complaints found for user"
        });
      }
      else {
        res.status(500).send({
          message: "Error retrieving user complaints"
        })
      }
    }
    else {
      res.status(200).send(data);
    }
  })
}

exports.findByMeterNumber = (req, res) => {
  if (!req.params.meternumber) {
    res.status(400).send({
      message: "No meternumber specified"
    })
  }
  CustomerComplain.findByMeterNumber(req.params.meternumber, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: "No complaints found for meter number"
        })
      }
      else {
        res.status(500).send({
          message: "Error retrieving complaints for meter number"
        })
      }
    }
    else {
      res.status(200).send(data);
    }
  })
}

exports.update = (req, res) => {
    if (!req.body) {
        res.status(400).send({message: "Content can not be empty!"});
    }

    CustomerComplain.updateById(req.params.id, new CustomerComplain(req.body), (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: `Complain Not found.`
              });
            } else {
              res.status(500).send({
                message: "Error updating complain"
              });
            }
          } else res.send(data);
    });
};

exports.delete = (req, res) => {
    CustomerComplain.remove(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: `Complain Not found.`
              });
            } else {
              res.status(500).send({
                message: "Could not delete complain"
              });
            }
          } else res.send({ message: `Complain was deleted successfully!` });
    });
};

//exports.create = (req, res) => {};