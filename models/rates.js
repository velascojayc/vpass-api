const sql = require("./db");

const Rate = function(rate) {
    this.name = rate.name,
    this.value = rate.value
}

Rate.getCategories = (result) => {
    sql.query(`SELECT * FROM config_category`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

Rate.getTypeAndConfig = (result) => {
    sql.query(`SELECT a.id AS typeid, b.id AS configid, a.configcategoryid AS categoryid, a.name AS type, b.config_key AS config FROM config_type a JOIN app_config b ON b.configtypeid = a.id`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

Rate.getRatesByCategoryPagination = (category, searchString, result) => {
    let addSearch = ``;
    if (searchString) {
        addSearch = `AND (a.name LIKE '%${searchString}%' OR b.config_key LIKE '%${searchString}%')`
    }
    sql.query(`SELECT a.name AS type, b.config_key AS config, c.app_config_value AS value, c.id AS appconfigid FROM config_type a JOIN app_config b ON b.configtypeid = a.id JOIN app_config_history c ON c.appconfigid = b.id WHERE a.configcategoryid = ${category} ${addSearch} order by c.id desc;`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

Rate.addRate = (appconfigid, value, result) => {
    const newRate = {
        appconfigid: appconfigid,
        app_config_value: value
    }
    sql.query(`INSERT INTO app_config_history SET ?`, newRate, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            result(null, res);
        }
    })
}
module.exports = Rate;