const sql = require("./db");

const ElectricMeter = function(electricMeter) {
    this.meter_number = electricMeter.meter_number,
    this.gps_lat = electricMeter.gps_lat,
    this.gps_lng = electricMeter.gps_lng
}


ElectricMeter.create = (newElectricMeter, result) => {
    sql.query(`INSERT INTO electric_meter SET ?`, newElectricMeter, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("created electric meter: ", {
            id: res.insertId,
            ...newElectricMeter
        });
        result(null, {
            id: res.insertId,
            ...newElectricMeter
        });
    });
};

ElectricMeter.findBy = (electricMeterId, result) => {
    sql.query(`SELECT a.*, b.accountnumber, c.firstname, c.lastname, c.email FROM electric_meter a JOIN user_account b ON a.id = b.electricmeterid JOIN app_user c ON c.id = b.appuserid WHERE a.id = ${electricMeterId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found electric meter: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({
            kind: "not_found"
        }, null);

    });
};

ElectricMeter.findByUserName = (username, result) => {
    sql.query(`SELECT a.* FROM electric_meter a JOIN user_account b ON b.electricmeterid = a.id JOIN app_user c ON c.id = b.appuserid WHERE c.username = '${username}' OR c.firstname LIKE '${username}%' OR c.lastname LIKE '${username}';`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res);
            return;
        } else {
            result({
                kind: "not_found"
            }, null);
        }
    });
}

ElectricMeter.findByUserId = (userId, result) => {
    sql.query(`SELECT a.* FROM electric_meter a JOIN user_account b ON b.electricmeterid = a.id WHERE b.appuserid = ${userId};`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    });
}

ElectricMeter.findByMeterNumber = (meternumber, result) => {
    sql.query(`SELECT * FROM electric_meter WHERE meter_number = ${meternumber}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res[0]);
            return
        }

        result({
            kind: "not_found"
        }, null);
    })
}

ElectricMeter.findMeterAccountNumber = (meternumber, result) => {
    sql.query(`SELECT a.accountnumber, b.* FROM user_account a JOIN electric_meter b ON b.id = a.electricmeterid WHERE b.meter_number = ${meternumber} AND a.is_active = 1`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res[0]);
            return
        }

        result({
            kind: "not_found"
        }, null);
    })
}

ElectricMeter.assignMeterToAccount = (meterId, accountId, result) => {
    sql.query(`UPDATE user_account SET electricmeterid = ${meterId} WHERE id = ${accountId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }

        result(null, res);
    })
}

ElectricMeter.getAll = result => {
    sql.query(`SELECT a.*, c.* FROM electric_meter a JOIN user_account b ON a.id = b.electricmeterid JOIN app_user c ON b.appuserid = c.id`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("app electric meters: ", res);
        result(null, res);
    });
};

ElectricMeter.search = (searchString, result) => {
    let searchQuery = '';
    if (searchString) {
        searchQuery = `WHERE a.meter_number LIKE '%${searchString}%'`;
    }
    sql.query(`SELECT a.*, c.username, b.accountnumber, b.is_active AS accountstatus, c.firstname, c.lastname FROM electric_meter a JOIN user_account b ON a.id = b.electricmeterid JOIN app_user c ON b.appuserid = c.id ${searchQuery}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("app electric meters: ", res);
        result(null, res);
    })
}

ElectricMeter.updateById = (id, electricMeter, result) => {
    sql.query(`UPDATE electric_meter SET meter_number = ?, gps_lat = ?, gps_lng = ? WHERE id = ?`, [
        electricMeter.meter_number, electricMeter.gps_lat, electricMeter.gps_lng, id
    ], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }

        console.log("updated electric meter: ", {
            id: id,
            ...electricMeter
        });

        result(null, {
            id: id,
            ...electricMeter
        })
    });
};

ElectricMeter.remove = (id, result) => {
    sql.query(`DELETE FROM electric_meter WHERE id = ?`, id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }

        console.log("deleted electric meter: ", id);

        result(null, id);

    });
}

module.exports = ElectricMeter;