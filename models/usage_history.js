const sql = require("./db");

const UsageHistory = function (usageHistory) { // this.id = usageHistory.id,
    this.useraccountid = usageHistory.useraccountid,
    this.usage = usageHistory.usage,
    this.past_reading = usageHistory.past_reading,
    this.current_reading = usageHistory.current_reading,
    this.usage_month = usageHistory.usage_month,
    this.usage_year = usageHistory.usage_year,
    this.amount_due = usageHistory.amount_due,
    this.received_by = usageHistory.received_by,
    this.meter_img = usageHistory.meter_img
}

UsageHistory.create = (newUsageHistory, result) => {
    sql.query(`SELECT id from app_config_history ORDER BY created_at DESC LIMIT 1`, (err, res) => {
        console.log("result from app_config_history: ", res[0].id);
        newUsageHistory['rate'] = res[0].id;
        sql.query(`INSERT INTO usage_history SET ?`, newUsageHistory, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }
    
            console.log("created usage history: ", {
                id: res.insertId,
                ...newUsageHistory
            });
            result(null, {
                id: res.insertId,
                ...newUsageHistory
            });
        });
    });
};

UsageHistory.findBy = (usageHistoryId, result) => {
    sql.query(`SELECT * FROM usage_history a WHERE a.id = ${usageHistoryId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found usage history: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({
            kind: "not_found"
        }, null);

    });
};

UsageHistory.validateExisting = (electricmeterid, month, year, result) => {
    sql.query(`SELECT * FROM usage_history a JOIN user_account b ON a.useraccountid = b.id WHERE b.electricmeterid = ${electricmeterid} AND a.usage_month = '${month}' AND a.usage_year = '${year}'`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found usage history: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({
            kind: "not_found"
        }, null);

    });
};

UsageHistory.findByAccountMonthYear = (accountnumber, month, year, result) => {
    sql.query(`SELECT a.* FROM usage_history a JOIN user_account b ON a.useraccountid = b.id WHERE b.accountnumber = '${accountnumber}' AND a.usage_month = '${month}' AND a.usage_year = '${year}'`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

UsageHistory.findByUserId = (userId, month, year, result) => {
    let queryStr = '';
    if (month && year) {
        queryStr = ` AND a.usage_year = '${year}' AND a.usage_month = '${month}'`;
    }
    sql.query(`SELECT a.* FROM usage_history a JOIN user_account b ON a.useraccountid = b.id WHERE b.appuserid = ${userId}` + queryStr, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
};

UsageHistory.findByUserAccountId = (useraccountid, result) => {
    sql.query(`SELECT * FROM usage_history WHERE useraccountid = ${useraccountid}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

UsageHistory.getLastTenByUserAccountId = (useraccountid, result) => {
    sql.query(`SELECT * FROM usage_history WHERE useraccountid = ${useraccountid} ORDER BY id DESC LIMIT 10`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        //if (res.length) {
            result(null, res);
            return;
        //}
        //result({
        //    kind: "not_found"
        //}, null);
    })
}

UsageHistory.findByUserAccountNumber = (accountnumber, result) => {
    sql.query(`SELECT a.* FROM usage_history a JOIN user_account b ON a.useraccountid = b.id WHERE b.accountnumber = '${accountnumber}'`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    })
}

UsageHistory.findByMeterId = (meterId, result) => {
    sql.query(`SELECT a.* FROM usage_history a JOIN user_account b ON a.useraccountid = b.id WHERE b.electricmeterid = ${meterId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    });
};

UsageHistory.search = (searchString, result) => {
    sql.query(`SELECT a.*, c.meter_number, d.id AS userid, d.username, d.firstname, d.lastname FROM usage_history a JOIN user_account b ON a.useraccountid = b.id JOIN electric_meter c ON b.electricmeterid = c.id JOIN app_user d ON b.appuserid = d.id WHERE d.firstname LIKE '%${searchString}%' OR d.lastname LIKE '%${searchString}%' OR c.meter_number LIKE '%${searchString}%'`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    });
};

UsageHistory.getAll = result => {
    sql.query(`SELECT a.*, c.meter_number, e.id AS userid, e.username, e.firstname, e.lastname FROM usage_history a JOIN user_account b ON a.useraccountid = b.id JOIN electric_meter c ON b.electricmeterid = c.id JOIN app_user e ON b.appuserid = e.id ORDER BY id DESC`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("usage histories: ", res);
        result(null, res);
        // return(null, res);
    });
};

UsageHistory.updateById = (id, usageHistory, result) => {
    let meterImg = '';
    let rate = '';
    if (usageHistory.meter_img) {
        meterImg = `, meter_img = '${usageHistory.meter_img}'`;
    }
    if (usageHistory.rate) {
        rate = `, rate = ${usageHistory.rate}`;
    }
    sql.query(`UPDATE usage_history SET useraccountid = ?, \`usage\` = ?, usage_month = ?, usage_year = ?, amount_due = ?, received_by = ?, past_reading = ?, current_reading = ?` + meterImg + rate + ` WHERE id = ?`, [
        usageHistory.useraccountid,
        usageHistory.usage,
        usageHistory.usage_month,
        usageHistory.usage_year,
        usageHistory.amount_due,
        usageHistory.received_by,
        usageHistory.past_reading,
        usageHistory.current_reading,
        parseInt(id)
    ], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log(res);
        if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }

        console.log("updated user: ", {
            id: id,
            ...usageHistory
        });

        result(null, {
            id: id,
            ...usageHistory
        })
    });
};

UsageHistory.remove = (id, result) => {
    sql.query(`DELETE FROM usage_history WHERE id = ?`, id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }

        console.log("deleted usage history: ", id);

        result(null, id);

    });
}

UsageHistory.getLatestRate = result => {
    sql.query(`SELECT * FROM app_config_history WHERE appconfigid = 1 ORDER BY id DESC LIMIT 1`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("latest rate: ", res[0]);
        result(null, res[0]);
    })
}

UsageHistory.getRateById = (id, result) => {
    sql.query(`SELECT * FROM app_config_history WHERE id = ${id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("latest rate: ", res[0]);
        result(null, res[0]);
    })
}

UsageHistory.getRates = (date, result) => {
    console.log(date);
    sql.query(`select a.app_config_value as value, b.config_key as rate, c.name as type, d.name as category from app_config_history a JOIN app_config b on a.appconfigid = b.id JOIN config_type c 
    on b.configtypeid = c.id join config_category d on c.configcategoryid = d.id WHERE a.id IN (SELECT max(id) as id FROM app_config_history WHERE created_at < '${date}' GROUP by appconfigid ORDER by id DESC) order by a.appconfigid;`, (err, res) => {
    // sql.query(`select a.app_config_value as value, b.config_key as rate, c.name as type, d.name as category from app_config_history a JOIN app_config b on a.appconfigid = b.id JOIN config_type c 
    // on b.configtypeid = c.id join config_category d on c.configcategoryid = d.id LEFT JOIN app_config_history z ON (a.appconfigid = z.appconfigid AND a.id < z.id) WHERE z.id IS NULL ${dateCheck}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, res);
    })
}

module.exports = UsageHistory;
