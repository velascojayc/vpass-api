const sql = require("./db");


const CustomerComplain = function (customerComplain) {
    this.id = customerComplain.id;
    this.useraccountid = customerComplain.useraccountid;
    this.title = customerComplain.title;
    this.description = customerComplain.description;
    this.status = customerComplain.status;
    this.assigned_to = customerComplain.assigned_to;
}

CustomerComplain.create = (newCustomerComplain, userid, result) => {
    sql.query(`INSERT INTO customer_complains SET ?`, newCustomerComplain, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            const newComplainHistory = {
                customercomplainid: res.insertId,
                new_status: 'In Progress',
                created_by: userid
            }
            sql.query(`INSERT INTO complain_history SET ?`, newComplainHistory, (err, res2) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                console.log("created customer complain: ", {
                    id: res.insertId,
                    ...newCustomerComplain
                });
                result(null, {
                    id: res.insertId,
                    ...newCustomerComplain
                });
            })
        }
    });
}; 

CustomerComplain.search = (searchTerm, result) => {
    sql.query(`SELECT a.*, b.accountnumber, c.username, c.firstname, c.lastname, d.meter_number FROM customer_complains a JOIN user_account b ON a.useraccountid = b.id JOIN app_user c ON c.id = b.appuserid JOIN electric_meter d ON d.id = b.electricmeterid WHERE a.title LIKE '%${searchTerm}%' OR a.description LIKE '%${searchTerm}%'`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found complaints");
            result(null, res);
            return;
        }

        result({
            kind: 'not_found'
        }, null);
    })
};

CustomerComplain.paginationSearch = (searchString, result) => {
    let searchQuery = '';
    if (searchString) {
        searchQuery = `WHERE a.title LIKE '%${searchString}%' OR a.description LIKE '%${searchString}%' OR c.username LIKE '%${searchString}%' OR b.accountnumber LIKE '%${searchString}%'`;
    }
    sql.query(`SELECT a.*, b.accountnumber, c.username, c.firstname, c.lastname, d.meter_number FROM customer_complains a JOIN user_account b ON a.useraccountid = b.id JOIN app_user c ON c.id = b.appuserid JOIN electric_meter d ON d.id = b.electricmeterid ${searchQuery}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found complaints");
            result(null, res);
            return;
        }

        result({
            kind: 'not_found'
        }, null);
    })
}

CustomerComplain.findBy = (customerComplainId, result) => {
    sql.query(`SELECT a.*, b.accountnumber, c.username, c.firstname, c.lastname, d.meter_number FROM customer_complains a JOIN user_account b ON a.useraccountid = b.id JOIN app_user c ON c.id = b.appuserid JOIN electric_meter d ON d.id = b.electricmeterid WHERE a.id = ${customerComplainId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found customer complain: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({
            kind: "not_found"
        }, null);
    });
};

CustomerComplain.findByUserId = (userId, result) => {
    sql.query(`SELECT a.*, b.accountnumber, c.username, c.firstname, c.lastname, d.meter_number FROM customer_complains a JOIN user_account b ON a.useraccountid = b.id JOIN app_user c ON c.id = b.appuserid JOIN electric_meter d ON d.id = b.electricmeterid WHERE a.useraccountid = ${userId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            result(null, res);
            return;
        }
        result({
            kind: "not_found"
        }, null);
    });
}

CustomerComplain.findByUserName = (username, result) => {
    let searchField = `c.username = ${username}`;
    if (isNaN(username)) {
        searchField = `c.firstname LIKE '${username}%' OR c.lastname LIKE '%${username}' OR c.username = '${username}'`;
    }
    sql.query(`SELECT a.*, b.accountnumber, c.username, c.firstname, c.lastname, d.meter_number FROM customer_complains a JOIN user_account b ON a.useraccountid = b.id JOIN app_user c ON c.id = b.appuserid JOIN electric_meter d ON d.id = b.electricmeterid WHERE ` + searchField, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res);
            return;
        }

        result({
            kind: "not_found"
        }, null);
    })
}

CustomerComplain.findByMeterNumber = (meternumber, result) => {
    sql.query(`SELECT a.*, b.accountnumber, c.username, c.firstname, c.lastname, d.meter_number FROM customer_complains a JOIN user_account b ON a.useraccountid = b.id JOIN app_user c ON c.id = b.appuserid JOIN electric_meter d ON d.id = b.electricmeterid WHERE d.meter_number = ${meternumber}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            result(null, res);
            return;
        }

        result({
            kind: "not_found"
        }, null);
    })
}

CustomerComplain.getAll = result => {
    sql.query(`SELECT a.*, b.accountnumber, c.username, c.firstname, c.lastname, d.meter_number FROM customer_complains a JOIN user_account b ON a.useraccountid = b.id JOIN app_user c ON c.id = b.appuserid JOIN electric_meter d ON d.id = b.electricmeterid`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("complains: ", res);
        result(null, res);
    });
};

CustomerComplain.updateById = (id, customerComplain, result) => {
    sql.query(`UPDATE customer_complains SET useraccountid = ?, title = ?, description = ?, status = ?, assigned_to = ?, updated_at = now() WHERE id = ?`, [
        customerComplain.useraccountid,
        customerComplain.title,
        customerComplain.description,
        customerComplain.status,
        customerComplain.assigned_to,
        id
    ], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }

        console.log("updated user: ", {
            id: id,
            ...customerComplain
        });

        result(null, {
            id: id,
            ...customerComplain
        })
    });
};

CustomerComplain.updateHistory = (userid, complainid, status, notes, result) => {
    sql.query(`UPDATE customer_complains SET status = '${status}' WHERE id = ${complainid}`, (err, updateComplain) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            sql.query(`SELECT * FROM complain_history WHERE customercomplainid = ${complainid} ORDER BY id DESC LIMIT 1`, (err, pastComplaint) =>{
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                if (pastComplaint.length) {
                    sql.query(`INSERT INTO complain_history SET customercomplainid = ?, old_status = ?, new_status = ?, created_by = ?, notes = ?`, [
                        complainid,
                        pastComplaint[0].new_status,
                        status,
                        userid,
                        notes
                    ], (err, res) => {
                        if (err) {
                            console.log("error: ", err);
                            result(err, null);
                            return;
                        }
                        else {
                            result(null, res);
                        }
                    })
                }
                else {
                    result({
                        kind: "not_found"
                    }, null);
                }
            })
        }
    })
}

CustomerComplain.getComplaintHistory = (id, result) => {
    sql.query(`SELECT a.old_status, a.new_status, a.created_at, a.created_by, a.notes, b.title, b.description, b.status, c.accountnumber FROM complain_history a JOIN customer_complains b ON a.customercomplainid = b.id JOIN user_account c ON c.id = b.useraccountid WHERE b.id = ${id} ORDER BY a.id DESC`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        else {
            result(null, res);
        }
    })
}

CustomerComplain.remove = (id, result) => {
    sql.query(`DELETE FROM customer_complains WHERE id = ?`, id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.affectedRows == 0) {
            result({
                kind: "not_found"
            }, null);
        }

        console.log("deleted complain: ", id);

        result(null, id);

    });
};


module.exports = CustomerComplain;