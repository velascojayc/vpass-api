const express = require('express'),
    app = express(),
    https = require('https'),
    http = require('http'),
    fs = require('fs'),
    bodyParser = require('body-parser'),
    router = require('./server/index.js'),
    mysql = require('mysql');
    swaggerui = require('swagger-ui-express');
    APIDocument = require('./documentation/swagger');
    AWS = require('aws-sdk');
    env = require('./config/env.config');
    cors = require('cors');

// AWS Config - uncomment below for AWS
AWS.config.update({
    secretAccessKey: env.S3_SECRETKEY,
    accessKeyId: env.S3_ACCESSKEY,
    signatureVersion: 'v4',
    region: env.S3_REGION
});

// const { check } = require('express-validator');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.json());
app.use('/api-docs', swaggerui.serve, swaggerui.setup(APIDocument));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(cors());

const hostname = 'localhost';
const port = 3000;

app.get("/", (req, res) => {
    res.json({ message: "Welcome to rnb api ." });
  });

require("./routes/app_user.route")(app);
require("./routes/customer_complain.route")(app);
require("./routes/electric_meter.route")(app);
require("./routes/usage_history.route")(app);
require("./routes/rates.route")(app);


app.listen(port, () => {
    console.log('RnB @ localhost:3000/');
});


