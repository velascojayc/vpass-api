module.exports = app => {
    const usageHistory = require("../controllers/usage_history.controller");
    const validation = require("../controllers/validation.controller");

    app.post("/usagehistory", [
        validation.verifyToken,
        usageHistory.create
    ]);

    app.get("/usagehistory", [
        validation.verifyToken,
        usageHistory.findByUserId
    ]);
    
    app.post("/usagehistory/check", [
        validation.verifyToken,
        validation.verifyAboveUser,
        usageHistory.checkExisting
    ]);

    app.get("/usagehistory/search/:searchString", [
        validation.verifyToken,
        validation.verifyAboveUser,
        usageHistory.search
    ]);

    app.get("/usagehistory/meter/:meterId", [
        validation.verifyToken,
        usageHistory.findByMeterId
    ]);

    app.get("/usagehistory/account/:accountnumber", [
        validation.verifyToken,
        usageHistory.findByAccountNumber
    ])

    app.get("/usagehistory/:usage_month/:usage_year", [
        validation.verifyToken,
        usageHistory.findByUserId
    ]);

    app.get("/usagehistory/:accountnumber/:usage_month/:usage_year", [
        validation.verifyToken,
        usageHistory.findByAccountDate
    ]);

    app.get("/usagehistory/all", [
        validation.verifyToken,
        validation.verifyAboveUser,
        usageHistory.findAll
    ]);

    app.get("/usagehistory/:id", [
        validation.verifyToken,
        usageHistory.findOne
    ]);

    app.put("/usagehistory/:id", [
        validation.verifyToken,
        usageHistory.update
    ]);
    
    app.delete("/usagehistory/:id", [
        validation.verifyToken,
        usageHistory.delete
    ]);

}