module.exports = app => {
    const customerComplain = require("../controllers/customer_complain.controller");
    const validation = require("../controllers/validation.controller");

    app.post("/complain", [
        validation.verifyToken,
        customerComplain.create
    ]);
    app.get("/complain", [
        validation.verifyToken,
        customerComplain.findByUserId
    ]);
    app.get("/complain/search/:search", [
        validation.verifyToken,
        customerComplain.search
    ]);
    app.get("/complain/all", [
        validation.verifyToken,
        customerComplain.findAll
    ]);
    app.post("/admin/complain/pagesearch", [
        validation.verifyToken,
        validation.verifyAdmin,
        customerComplain.paginationSearch
    ]);
    app.post("/admin/complain/updatehistory", [
        validation.verifyToken,
        validation.verifyAdmin,
        customerComplain.updateHistory
    ]);
    app.get("/admin/complain/gethistory/:id", [
        validation.verifyToken,
        validation.verifyAdmin,
        customerComplain.getComplaintHistory
    ])
    app.get("/complain/meter/:meternumber", [
        validation.verifyToken,
        validation.verifyAboveUser,
        customerComplain.findByMeterNumber
    ]);
    app.get("/complain/:id", [
        validation.verifyToken,
        customerComplain.findOne
    ]);
    app.get("/complain/user/:username", [
        validation.verifyToken,
        validation.verifyAboveUser,
        customerComplain.findByUserName
    ]);
    app.put("/complain/:id", [
        validation.verifyToken,
        customerComplain.update
    ]);
    app.delete("/complain/:id", [
        validation.verifyToken,
        customerComplain.delete
    ]);
}