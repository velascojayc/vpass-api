module.exports = app => {
    const rates = require("../controllers/rates.controller");
    const validation = require("../controllers/validation.controller");

    app.post("/rates/getbycategory", [
        validation.verifyToken,
        validation.verifyAdmin,
        rates.getRatesPagination
    ]);

    app.get("/rates/categories", [
        validation.verifyToken,
        validation.verifyAdmin,
        rates.getRateCategories
    ]);

    app.get("/rates/types", [
        validation.verifyToken,
        validation.verifyAdmin,
        rates.getTypeAndConfig
    ]);

    app.post("/rates/addrate", [
        validation.verifyToken,
        validation.verifyAdmin,
        rates.addRate
    ])
}