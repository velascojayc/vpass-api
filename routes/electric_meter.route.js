module.exports = app => {
    const electricMeter = require("../controllers/electric_meter.controller");
    const validation = require("../controllers/validation.controller");

    app.post("/electricmeter", [
        validation.verifyToken,
        electricMeter.create
    ]);

    app.get("/electricmeter", [
        validation.verifyToken,
        electricMeter.findByUserId
    ]);
    app.get("/electricmeter/all", [
        validation.verifyToken,
        electricMeter.findAll
    ]);
    app.post("/electricmeter/addaccount", [
        validation.verifyToken,
        validation.verifyAdmin,
        electricMeter.addMeterToAccount
    ])
    app.get("/electricmeter/:id", [
        validation.verifyToken,
        electricMeter.findOne
    ]);
    app.get("/electricmeter/user/:username", [
        validation.verifyToken,
        validation.verifyAboveUser,
        electricMeter.findByUserName
    ]);
    app.post("/admin/electricmeter/pagesearch", [
        validation.verifyToken,
        validation.verifyAdmin,
        electricMeter.paginationSearch
    ])
    app.get("/electricmeter/meter/:meternumber", [
        validation.verifyToken,
        validation.verifyAboveUser,
        electricMeter.findByMeterNumber
    ]);
    app.put("/electricmeter/:id", [
        validation.verifyToken,
        electricMeter.update
    ]);

    app.delete("/electricmeter/:id", [
        validation.verifyToken,
        electricMeter.delete
    ]);
}